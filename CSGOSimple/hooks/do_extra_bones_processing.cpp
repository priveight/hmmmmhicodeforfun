﻿#include "hooks.hpp"
#include "listener_entity.h"
#include "..\features\features.h"
#include "../helpers/console.h"

int __fastcall hooks::hk_calc_view ( void* ecx, double st0, int Angle, int a3, int a4, int a5, float* a6 )
{
    const auto e = reinterpret_cast< C_BasePlayer* > ( ecx );

    return g_listener_entity.m_track[ e->ent_index( ) ].vmt.get_original< hooks::calc_view_t > ( index::calc_view ) ( ecx, st0, Angle, a3, a4, a5, a6 );
}

void __fastcall hooks::hk_do_extra_bones_processing ( void* ecx, void*, CStudioHdr* hdr, Vector* pos, quaternion_t* q, matrix3x4_t* matrix,
                                                      void* bone_list, void* context )
{
	return;
}
