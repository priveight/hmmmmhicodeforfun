#include "resolver_recode.h"
#include "../../../helpers/console.h"

#include "../../../helpers/debug_console.h"

#include "../../../helpers/math.hpp"

#include "../../../options.h"

#include "../../features.h"

#include <algorithm>

recode_resolver RECODEETYXYETY; // ����� ��� � ���� �����, ����� �������� ����� ��������, �� �� ������� ������ � ��� ��������� ����� ����������

//��� �� �� ����� �� ������ (������ - animstate = nullptr)

void recode_resolver::HandleHits(C_BasePlayer* pEnt) //def handle
{
	auto NetChannel = interfaces::engine_client->get_net_channel_info();

	if (!NetChannel)
		return;

	static float predTime[65];
	static bool init[65];

	if (Shot[pEnt->ent_index()])
	{
		if (init[pEnt->ent_index()])
		{
			Pitchhit[pEnt->ent_index()] = pEnt->eye_angles().pitch;
			predTime[pEnt->ent_index()] = interfaces::global_vars->curtime + NetChannel->GetAvgLatency(FLOW_INCOMING) + NetChannel->GetAvgLatency(FLOW_OUTGOING) + ticks_to_time(1) + ticks_to_time(interfaces::client_state->chokedcommands);
			init[pEnt->ent_index()] = false;
		}

		if (interfaces::global_vars->curtime > predTime[pEnt->ent_index()] && !Hit[pEnt->ent_index()])
		{
			missedshots[pEnt->ent_index()] += 1;
			Shot[pEnt->ent_index()] = false;
		}
		else if (interfaces::global_vars->curtime <= predTime[pEnt->ent_index()] && Hit[pEnt->ent_index()])
			Shot[pEnt->ent_index()] = false;

	}
	else
		init[pEnt->ent_index()] = true;

	Hit[pEnt->ent_index()] = false;
}

float flAngleMod(float flAngle)
{
	return((360.0f / 65536.0f) * ((int32_t)(flAngle * (65536.0f / 360.0f)) & 65535));
}
float ApproachAngle(float target, float value, float speed)
{
	target = flAngleMod(target);
	value = flAngleMod(value);

	float delta = target - value;

	// Speed is assumed to be positive
	if (speed < 0)
		speed = -speed;

	if (delta < -180)
		delta += 360;
	else if (delta > 180)
		delta -= 360;

	if (delta > speed)
		value += speed;
	else if (delta < -speed)
		value -= speed;
	else
		value = target;

	return value;
}
/*


*/

void recode_resolver::resolver_recode()
{
	for (int i = 0; i < interfaces::engine_client->get_max_clients(); i++)
	{

		const auto entity = dynamic_cast<C_BasePlayer*>(interfaces::entity_list->get_client_entity(i));

		if (!entity || !entity->is_alive() || entity->is_dormant())
			continue;

		auto animState = entity->get_base_player_anim_state();

		auto goal_feet_yaw = animState->m_flGoalFeetYaw;

		auto angle_diff = [](float destAngle, float srcAngle) -> float
		{
			auto delta = 0.f;

			delta = fmodf(destAngle - srcAngle, 360.0f);

			if (destAngle > srcAngle)
			{
				if (delta >= 180)
					delta -= 360;
			}
			else
			{
				if (delta <= -180)
					delta += 360;
			}

			return delta;
		};

		auto flEyeYaww = animState->m_flYaw;

		const auto duckammount = *reinterpret_cast<float*> (animState + 0xA4);

		auto eye_feet_delta = angle_diff(flEyeYaww, goal_feet_yaw);

		static auto GetSmoothedVelocity = [](float min_delta, Vector a, Vector b) {
			Vector delta = a - b;
			float delta_length = delta.length();

			if (delta_length <= min_delta) {
				Vector result;
				if (-min_delta <= delta_length) {
					return a;
				}
				else {
					float iradius = 1.0f / (delta_length + FLT_EPSILON + 33);
					return b - ((delta * iradius) * min_delta);
				}
			}
			else {
				float iradius = 1.0f / (delta_length + FLT_EPSILON + 33); // �� ���
				return b + ((delta * iradius) * min_delta);
			}
		};
		float FakeGoalYaw;
		float v25 = std::clamp(animState->m_fLandingDuckAdditiveSomething + animState->m_fDuckAmount, 0.0f, 1.0f);
		float v26 = animState->m_fDuckAmount;
		float m_flChokedTime = entity->simulation_time() - (entity->simulation_time() + 4);
		float v27 = m_flChokedTime * 6.0f;
		float v28;

		// clamp 
		if ((v25 - v26) <= v27) {
			if (-v27 <= (v25 - v26))
				v28 = v25;
			else
				v28 = v26 - v27;
		}
		else {
			v28 = v26 + v27;
		}

		Vector velocity = entity->vec_velocity();
		float flDuckAmount = 0.f;
		flDuckAmount = std::clamp(v28, 0.0f, 1.0f);

		Vector animationVelocity = GetSmoothedVelocity(m_flChokedTime * 2000.0f, velocity, reinterpret_cast<CCSGOPlayerAnimState*>(animState)->get_vec_velocity());
		float speed = std::fminf(animationVelocity.length(), 260.0f);

		auto weapon = (CCSWeaponInfo*)entity->active_weapon().get();

		float flMaxMovementSpeed = 260.0f;
		if (weapon) {
			flMaxMovementSpeed = std::fmaxf(weapon->max_speed, 0.001f);
		}

		float flRunningSpeed = speed / (flMaxMovementSpeed * 0.520f);
		float flDuckingSpeed = speed / (flMaxMovementSpeed * 0.340f);

		flRunningSpeed = std::clamp(flRunningSpeed, 0.0f, 1.0f);

		float flYawModifier = (((*(float*)(uintptr_t(animState) + 0x11c) * -0.3f) - 0.2f) * flRunningSpeed) + 1.0f;
		if (flDuckAmount > 0.0f) {
			float flDuckingSpeed = std::clamp(flDuckingSpeed, 0.f, 1.f);
			flYawModifier += (flDuckAmount * flDuckingSpeed) * (0.5f - flYawModifier);
		}

		float m_flMaxBodyYaw = *(float*)(uintptr_t(animState) + 0x334) * flYawModifier;
		float m_flMinBodyYaw = *(float*)(uintptr_t(animState) + 0x330) * flYawModifier;

		float flEyeYaw = entity->eye_angles().yaw;
		float flEyeDiff = std::remainderf(flEyeYaw - FakeGoalYaw, 360.f);

		if (flEyeDiff <= m_flMaxBodyYaw) {
			if (m_flMinBodyYaw > flEyeDiff)
				FakeGoalYaw = fabs(m_flMinBodyYaw) + flEyeYaw;
		}
		else {
			FakeGoalYaw = flEyeYaw - fabs(m_flMaxBodyYaw);
		}

		FakeGoalYaw = std::remainderf(FakeGoalYaw, 360.f);

		if (speed > 0.1f || fabs(velocity.z) > 100.0f) {
			FakeGoalYaw = ApproachAngle(
				flEyeYaw,
				FakeGoalYaw,
				((*(float*)(uintptr_t(animState) + 0x11c) * 20.0f) + 30.0f)
				* m_flChokedTime);
		}
		else {
			FakeGoalYaw = ApproachAngle(
				entity->lower_body_yaw_target(),
				FakeGoalYaw,
				m_flChokedTime * 100.0f);
		}

		float Left = flEyeYaw - m_flMinBodyYaw;
		float Right = flEyeYaw - m_flMaxBodyYaw;

		LeftGlobal = flEyeYaw - m_flMinBodyYaw;
		RightGlobal = flEyeYaw - m_flMaxBodyYaw;
		FakeGoall = FakeGoalYaw;

		animState->m_flGoalFeetYaw = Left;
	}
}

void recode_resolver::FrameStage(ClientFrameStage_t stage)
{
	if (!interfaces::engine_client->is_in_game())
		return;

	for (int i = 0; i < interfaces::engine_client->get_max_clients(); ++i)
	{
		const auto player = dynamic_cast<C_BasePlayer*> (interfaces::entity_list->get_client_entity(i));

		if (!player || !player->is_alive() || player->is_dormant())
			continue;

	}
}